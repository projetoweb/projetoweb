DROP TABLE IF EXISTS `estoque`.`users` ;

CREATE TABLE users
(
username VARCHAR(15) NOT NULL,
password VARCHAR(40),
authority VARCHAR(15),
PRIMARY KEY (username)
);

INSERT INTO users (username,password,authority) VALUES ('jose','1234','ROLE_ADMIN');
INSERT INTO users (username,password,authority) VALUES ('silvio','1234','ROLE_USER');