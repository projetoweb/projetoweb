/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Movimento;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import modelo.TipoMovimento;
import modelo.dao.exceptions.IllegalOrphanException;
import modelo.dao.exceptions.NonexistentEntityException;
import modelo.dao.exceptions.RollbackFailureException;

/**
 *
 * @author usuario
 */
public class TipoMovimentoJpaController implements Serializable {

    public TipoMovimentoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoMovimento tipoMovimento) throws RollbackFailureException, Exception {
        if (tipoMovimento.getMovimentoList() == null) {
            tipoMovimento.setMovimentoList(new ArrayList<Movimento>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Movimento> attachedMovimentoList = new ArrayList<Movimento>();
            for (Movimento movimentoListMovimentoToAttach : tipoMovimento.getMovimentoList()) {
                movimentoListMovimentoToAttach = em.getReference(movimentoListMovimentoToAttach.getClass(), movimentoListMovimentoToAttach.getCodMovimento());
                attachedMovimentoList.add(movimentoListMovimentoToAttach);
            }
            tipoMovimento.setMovimentoList(attachedMovimentoList);
            em.persist(tipoMovimento);
            for (Movimento movimentoListMovimento : tipoMovimento.getMovimentoList()) {
                TipoMovimento oldCodTipoOfMovimentoListMovimento = movimentoListMovimento.getCodTipo();
                movimentoListMovimento.setCodTipo(tipoMovimento);
                movimentoListMovimento = em.merge(movimentoListMovimento);
                if (oldCodTipoOfMovimentoListMovimento != null) {
                    oldCodTipoOfMovimentoListMovimento.getMovimentoList().remove(movimentoListMovimento);
                    oldCodTipoOfMovimentoListMovimento = em.merge(oldCodTipoOfMovimentoListMovimento);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoMovimento tipoMovimento) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoMovimento persistentTipoMovimento = em.find(TipoMovimento.class, tipoMovimento.getCodTipo());
            List<Movimento> movimentoListOld = persistentTipoMovimento.getMovimentoList();
            List<Movimento> movimentoListNew = tipoMovimento.getMovimentoList();
            List<String> illegalOrphanMessages = null;
            for (Movimento movimentoListOldMovimento : movimentoListOld) {
                if (!movimentoListNew.contains(movimentoListOldMovimento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Movimento " + movimentoListOldMovimento + " since its codTipo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Movimento> attachedMovimentoListNew = new ArrayList<Movimento>();
            for (Movimento movimentoListNewMovimentoToAttach : movimentoListNew) {
                movimentoListNewMovimentoToAttach = em.getReference(movimentoListNewMovimentoToAttach.getClass(), movimentoListNewMovimentoToAttach.getCodMovimento());
                attachedMovimentoListNew.add(movimentoListNewMovimentoToAttach);
            }
            movimentoListNew = attachedMovimentoListNew;
            tipoMovimento.setMovimentoList(movimentoListNew);
            tipoMovimento = em.merge(tipoMovimento);
            for (Movimento movimentoListNewMovimento : movimentoListNew) {
                if (!movimentoListOld.contains(movimentoListNewMovimento)) {
                    TipoMovimento oldCodTipoOfMovimentoListNewMovimento = movimentoListNewMovimento.getCodTipo();
                    movimentoListNewMovimento.setCodTipo(tipoMovimento);
                    movimentoListNewMovimento = em.merge(movimentoListNewMovimento);
                    if (oldCodTipoOfMovimentoListNewMovimento != null && !oldCodTipoOfMovimentoListNewMovimento.equals(tipoMovimento)) {
                        oldCodTipoOfMovimentoListNewMovimento.getMovimentoList().remove(movimentoListNewMovimento);
                        oldCodTipoOfMovimentoListNewMovimento = em.merge(oldCodTipoOfMovimentoListNewMovimento);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoMovimento.getCodTipo();
                if (findTipoMovimento(id) == null) {
                    throw new NonexistentEntityException("The tipoMovimento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoMovimento tipoMovimento;
            try {
                tipoMovimento = em.getReference(TipoMovimento.class, id);
                tipoMovimento.getCodTipo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoMovimento with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Movimento> movimentoListOrphanCheck = tipoMovimento.getMovimentoList();
            for (Movimento movimentoListOrphanCheckMovimento : movimentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoMovimento (" + tipoMovimento + ") cannot be destroyed since the Movimento " + movimentoListOrphanCheckMovimento + " in its movimentoList field has a non-nullable codTipo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoMovimento);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoMovimento> findTipoMovimentoEntities() {
        return findTipoMovimentoEntities(true, -1, -1);
    }

    public List<TipoMovimento> findTipoMovimentoEntities(int maxResults, int firstResult) {
        return findTipoMovimentoEntities(false, maxResults, firstResult);
    }

    private List<TipoMovimento> findTipoMovimentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoMovimento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoMovimento findTipoMovimento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoMovimento.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoMovimentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoMovimento> rt = cq.from(TipoMovimento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
