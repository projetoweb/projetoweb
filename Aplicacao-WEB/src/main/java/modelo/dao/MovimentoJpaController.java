/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import modelo.Movimento;
import modelo.TipoMovimento;
import modelo.Produto;
import modelo.dao.exceptions.NonexistentEntityException;
import modelo.dao.exceptions.RollbackFailureException;

/**
 *
 * @author usuario
 */
public class MovimentoJpaController implements Serializable {

    public MovimentoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Movimento movimento) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TipoMovimento codTipo = movimento.getCodTipo();
            if (codTipo != null) {
                codTipo = em.getReference(codTipo.getClass(), codTipo.getCodTipo());
                movimento.setCodTipo(codTipo);
            }
            Produto codProduto = movimento.getCodProduto();
            if (codProduto != null) {
                codProduto = em.getReference(codProduto.getClass(), codProduto.getCodProduto());
                movimento.setCodProduto(codProduto);
            }
            em.persist(movimento);
            if (codTipo != null) {
                codTipo.getMovimentoList().add(movimento);
                codTipo = em.merge(codTipo);
            }
            if (codProduto != null) {
                codProduto.getMovimentoList().add(movimento);
                codProduto = em.merge(codProduto);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Movimento movimento) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Movimento persistentMovimento = em.find(Movimento.class, movimento.getCodMovimento());
            TipoMovimento codTipoOld = persistentMovimento.getCodTipo();
            TipoMovimento codTipoNew = movimento.getCodTipo();
            Produto codProdutoOld = persistentMovimento.getCodProduto();
            Produto codProdutoNew = movimento.getCodProduto();
            if (codTipoNew != null) {
                codTipoNew = em.getReference(codTipoNew.getClass(), codTipoNew.getCodTipo());
                movimento.setCodTipo(codTipoNew);
            }
            if (codProdutoNew != null) {
                codProdutoNew = em.getReference(codProdutoNew.getClass(), codProdutoNew.getCodProduto());
                movimento.setCodProduto(codProdutoNew);
            }
            movimento = em.merge(movimento);
            if (codTipoOld != null && !codTipoOld.equals(codTipoNew)) {
                codTipoOld.getMovimentoList().remove(movimento);
                codTipoOld = em.merge(codTipoOld);
            }
            if (codTipoNew != null && !codTipoNew.equals(codTipoOld)) {
                codTipoNew.getMovimentoList().add(movimento);
                codTipoNew = em.merge(codTipoNew);
            }
            if (codProdutoOld != null && !codProdutoOld.equals(codProdutoNew)) {
                codProdutoOld.getMovimentoList().remove(movimento);
                codProdutoOld = em.merge(codProdutoOld);
            }
            if (codProdutoNew != null && !codProdutoNew.equals(codProdutoOld)) {
                codProdutoNew.getMovimentoList().add(movimento);
                codProdutoNew = em.merge(codProdutoNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = movimento.getCodMovimento();
                if (findMovimento(id) == null) {
                    throw new NonexistentEntityException("The movimento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Movimento movimento;
            try {
                movimento = em.getReference(Movimento.class, id);
                movimento.getCodMovimento();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The movimento with id " + id + " no longer exists.", enfe);
            }
            TipoMovimento codTipo = movimento.getCodTipo();
            if (codTipo != null) {
                codTipo.getMovimentoList().remove(movimento);
                codTipo = em.merge(codTipo);
            }
            Produto codProduto = movimento.getCodProduto();
            if (codProduto != null) {
                codProduto.getMovimentoList().remove(movimento);
                codProduto = em.merge(codProduto);
            }
            em.remove(movimento);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Movimento> findMovimentoEntities() {
        return findMovimentoEntities(true, -1, -1);
    }

    public List<Movimento> findMovimentoEntities(int maxResults, int firstResult) {
        return findMovimentoEntities(false, maxResults, firstResult);
    }

    private List<Movimento> findMovimentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Movimento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Movimento findMovimento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Movimento.class, id);
        } finally {
            em.close();
        }
    }

    public int getMovimentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Movimento> rt = cq.from(Movimento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
