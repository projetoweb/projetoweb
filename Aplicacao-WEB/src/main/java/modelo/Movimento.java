/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "movimento")
@NamedQueries({
    @NamedQuery(name = "Movimento.findAll", query = "SELECT m FROM Movimento m")})
public class Movimento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Cod_Movimento")
    private Integer codMovimento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Quantidade")
    private int quantidade;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Data_Cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;
    @JoinColumn(name = "Cod_Tipo", referencedColumnName = "Cod_Tipo")
    @ManyToOne(optional = false)
    private TipoMovimento codTipo;
    @JoinColumn(name = "Cod_Produto", referencedColumnName = "Cod_Produto")
    @ManyToOne(optional = false)
    private Produto codProduto;

    public Movimento() {
    }

    public Movimento(Integer codMovimento) {
        this.codMovimento = codMovimento;
    }

    public Movimento(Integer codMovimento, int quantidade, Date dataCadastro) {
        this.codMovimento = codMovimento;
        this.quantidade = quantidade;
        this.dataCadastro = dataCadastro;
    }

    public Integer getCodMovimento() {
        return codMovimento;
    }

    public void setCodMovimento(Integer codMovimento) {
        this.codMovimento = codMovimento;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public TipoMovimento getCodTipo() {
        return codTipo;
    }

    public void setCodTipo(TipoMovimento codTipo) {
        this.codTipo = codTipo;
    }

    public Produto getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(Produto codProduto) {
        this.codProduto = codProduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codMovimento != null ? codMovimento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movimento)) {
            return false;
        }
        Movimento other = (Movimento) object;
        if ((this.codMovimento == null && other.codMovimento != null) || (this.codMovimento != null && !this.codMovimento.equals(other.codMovimento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.sicemal.aplicacao.web.Movimento[ codMovimento=" + codMovimento + " ]";
    }
    
}
