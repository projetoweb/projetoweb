/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beanSessao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Movimento;

/**
 *
 * @author usuario
 */
@Stateless
public class MovimentoFacade extends AbstractFacade<Movimento> {
    @PersistenceContext(unitName = "br.com.sicemal_Aplicacao-WEB_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MovimentoFacade() {
        super(Movimento.class);
    }
    
}
