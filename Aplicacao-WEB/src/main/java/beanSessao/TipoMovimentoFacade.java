/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beanSessao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.TipoMovimento;

/**
 *
 * @author usuario
 */
@Stateless
public class TipoMovimentoFacade extends AbstractFacade<TipoMovimento> {
    @PersistenceContext(unitName = "br.com.sicemal_Aplicacao-WEB_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoMovimentoFacade() {
        super(TipoMovimento.class);
    }
    
}
