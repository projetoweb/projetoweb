SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `estoque` ;
CREATE SCHEMA IF NOT EXISTS `estoque` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


SHOW WARNINGS;
USE `estoque` ;

-- -----------------------------------------------------
-- Table `estoque`.`PRODUTO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `estoque`.`PRODUTO` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `estoque`.`PRODUTO` (
  `Cod_Produto` INT NOT NULL AUTO_INCREMENT ,
  `Des_Nome` VARCHAR(50) NOT NULL ,
  `Estoque_Atual` INT DEFAULT '0' ,
  `Estoque_Minimo` INT NULL ,
  `Estoque_Maximo` INT NULL ,
  `Custo_Unitario` DECIMAL(14,2) NOT NULL ,
  `Custo_Venda` DECIMAL(14,2) NOT NULL ,
  `Apresentacao` VARCHAR(2) NOT NULL ,
  PRIMARY KEY (`Cod_Produto`) ) 
DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `estoque`.`TIPO_MOVIMENTO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `estoque`.`TIPO_MOVIMENTO` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `estoque`.`TIPO_MOVIMENTO` (
  `Cod_Tipo` INT NOT NULL AUTO_INCREMENT ,
  `Tipo` CHAR(1) NOT NULL ,
  `Descricao` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Cod_Tipo`) )
DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `estoque`.`MOVIMENTO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `estoque`.`MOVIMENTO` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `estoque`.`MOVIMENTO` (
  `Cod_Movimento` INT NOT NULL AUTO_INCREMENT ,
  `Cod_Produto` INT NOT NULL ,
  `Cod_Tipo` INT NOT NULL ,
  `Quantidade` INT NOT NULL ,
  `Data_Cadastro` DATETIME NOT NULL ,
  PRIMARY KEY (`Cod_Movimento`) ,
  CONSTRAINT `fkMovimento_TipoMovimento`
    FOREIGN KEY (`Cod_Tipo` )
    REFERENCES `estoque`.`TIPO_MOVIMENTO` (`Cod_Tipo` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fkMovimento_Produto`
    FOREIGN KEY (`Cod_Produto` )
    REFERENCES `estoque`.`PRODUTO` (`Cod_Produto` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fkMovimento_TipoMovimento_idx` ON `estoque`.`MOVIMENTO` (`Cod_Tipo` ASC) ;

SHOW WARNINGS;
CREATE INDEX `fkMovimento_Produto_idx` ON `estoque`.`MOVIMENTO` (`Cod_Produto` ASC) ;

SHOW WARNINGS;
USE `estoque` ;


ALTER TABLE produto DEFAULT CHARACTER SET UTF8 COLLATE utf8_unicode_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
