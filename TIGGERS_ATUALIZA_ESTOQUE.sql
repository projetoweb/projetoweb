DELIMITER //


DROP TRIGGER IF EXISTS TRI_ATU_ESTOQUE_INSERT //

CREATE TRIGGER TRI_ATU_ESTOQUE_INSERT
BEFORE INSERT ON MOVIMENTO
FOR EACH ROW BEGIN
  DECLARE estoque INT;
  DECLARE operacao CHAR;
  DECLARE estoqueAtual INT;

  SET operacao = (SELECT UPPER(Tipo) FROM tipo_movimento 
    WHERE Cod_Tipo = NEW.Cod_Tipo);

  SET estoque = (SELECT Estoque_Atual FROM produto 
    WHERE Cod_Produto = NEW.Cod_Produto);

  IF (operacao = 'E') THEN
    UPDATE produto SET Estoque_Atual = estoque + NEW.Quantidade
      WHERE Cod_Produto = NEW.Cod_Produto;
  ELSE

    IF (operacao = 'S') THEN
      IF (estoque >= NEW.Quantidade) THEN
        SET estoque = estoque - NEW.Quantidade;
        UPDATE produto SET Estoque_Atual = estoque
          WHERE Cod_Produto = NEW.Cod_Produto;
	  ELSE
		SET NEW = 'Quantidade em estoque insuficiente.';
	  END IF;
    END IF;
  END IF;
END//

DROP TRIGGER IF EXISTS TRI_ATU_ESTOQUE_DELETE //

CREATE TRIGGER TRI_ATU_ESTOQUE_DELETE
BEFORE DELETE ON MOVIMENTO
FOR EACH ROW BEGIN
  DECLARE estoqueAtual INT;
  SET estoqueAtual = (SELECT Estoque_Atual FROM produto 
    WHERE Cod_Produto = OLD.Cod_Produto) - OLD.Quantidade;
  UPDATE produto SET Estoque_Atual = estoqueAtual
    WHERE Cod_Produto = OLD.Cod_Produto;
END//


DROP TRIGGER IF EXISTS TRI_ATU_ESTOQUE_UPDATE //

CREATE TRIGGER TRI_ATU_ESTOQUE_UPDATE
BEFORE UPDATE ON MOVIMENTO
FOR EACH ROW BEGIN
  DECLARE estoque INT;
  DECLARE operacao CHAR;

  SET operacao = (SELECT UPPER(Tipo) FROM tipo_movimento 
    WHERE Cod_Tipo = NEW.Cod_Tipo);
  SET estoque = (SELECT Estoque_Atual FROM produto 
    WHERE Cod_Produto = NEW.Cod_Produto);

  IF (operacao = 'E') THEN
    SET estoque = estoque - OLD.Quantidade;
    SET estoque = estoque + NEW.Quantidade;
    UPDATE produto SET Estoque_Atual = estoque
      WHERE Cod_Produto = NEW.Cod_Produto;
  ELSE
    IF (operacao = 'S') THEN
      SET estoque = estoque + OLD.Quantidade;
      IF (estoque >= NEW.Quantidade) THEN
        SET estoque = estoque - NEW.Quantidade;
        UPDATE produto SET Estoque_Atual = estoque
          WHERE Cod_Produto = NEW.Cod_Produto;
	  ELSE
		SET NEW = 'Quantidade em estoque insuficiente.';
	  END IF;
    END IF;
  END IF;

END//